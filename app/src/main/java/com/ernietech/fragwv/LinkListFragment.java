package com.ernietech.fragwv;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.homsy on 11/4/2014.
 */
public class LinkListFragment extends Fragment {

    private static List<LinkData> linkDataList = new ArrayList<LinkData>();
    private LinkAdapter la;

    static {
        linkDataList.add(new LinkData("SwA", "http://www.survivingwithandroid.com"));

        linkDataList.add(new LinkData("Android", "http://www.android.com"));
        linkDataList.add(new LinkData("Google Mail", "http://mail.google.com"));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Here we set our custom adapter. Now we have the reference to the activity

    }


    public LinkListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("SwA", "LV onCreateView");
        View v = inflater.inflate(R.layout.linklist_layout, container, false);
        ListView lv = (ListView) v.findViewById(R.id.urls);
        la = new LinkAdapter(linkDataList, getActivity());
        lv.setAdapter(la);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
        public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id){
                LinkData data = ((LinkAdapter)la).getItem(position);
                ( (ChangeLinkListener)  getActivity()).onLinkChange(data.getLink());


            }
        });
        return v;
    }
}
