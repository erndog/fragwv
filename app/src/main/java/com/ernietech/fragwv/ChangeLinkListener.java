package com.ernietech.fragwv;

/**
 * Created by e.homsy on 11/4/2014.
 */
public interface ChangeLinkListener {
    public void onLinkChange(String link);
}
